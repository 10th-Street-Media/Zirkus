# Zirkus

Zirkus is a modern take on forum software. By including ActivityPub, Zirkus allows forums to expand across the Fediverse, making it easier for people to like, dislike, share, and comment on posts. The Internet has come a long way in the past few decades. It's time to for bulletin boards to catch up.
